"""
avatar_feature_data.py
-----------------------------------------
Author: Kristin Brooks
-----------------------------------------
This file contains the data for the colors and styles used to make the avatars in eight_bit_avatar_gui_version.py
"""

SKIN_COLORS = [('#FAE6DB', '#EEA2A9'),
               ('#d3aa91', '#CA8D7C'),
               ('#ba8762', '#b16b47'),
               ('#4c2e1e', '#47271a'),
               ('#31261D', '#40342B')
               ]

EYE_COLORS = ['#88CBF1',
              '#5C3201',
              '#6F9061',
              '#939393',
              '#BB823E'
              ]

HAIR_COLORS = ['#F6E49A',
               '#A77D5D',
               '#584129',
               '#000000',
               '#CA7429',
               '#C1C1C1',
               '#FC01C7',
               '#610882',
               '#1F4EFD',
               '#41D002',
               '#FCF401',
               '#ff6523',
               '#DE0101'
               ]

HAIR_STYLES = ['short',
               'medium',
               'long',
               'ponytail',
               'pigtails',
               'bald',
               'mohawk'
               ]
TOP_STYLES = ['plain',
              'striped',
              'suspenders',
              'jacket'
              ]

