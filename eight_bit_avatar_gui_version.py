"""
eight_bit_avatar_gui_version.py
----------------------------------------
Author: Kristin Brooks
----------------------------------------
Final Project for Stanford's Code in Place Spring 2020
----------------------------------------
This program opens a window with a canvas on the left and a frame of buttons on the right. It starts with a
basic avatar drawn on the canvas. Then the user can click the buttons on the right to change the colors and styles
of the avatar's features. When they are done they can optionally click the Save button the save the canvas image
and then click the Close button to close the window.
"""

import io
from tkinter import Frame, Button, Canvas, RIGHT, LEFT, TOP, BOTH, Label, RAISED, BOTTOM, Checkbutton, X, Tk, CENTER
from PIL import Image
from tkmacosx import Button
from avatar_draw_functions import *
from avatar_feature_data import *

CANVAS_WIDTH = 800
CANVAS_HEIGHT = 800
FRAME_BACKGROUND_COLOR = '#02CAA8'
LABEL_COLOR = '#A2DE02'
BUTTON_COLOR = '#03fcd3'
BUTTON_FRAME_WIDTH = 150
BUTTON_FRAME_HEIGHT = 800


class AvatarGui:
    def __init__(self, window):
        self.window = window
        self.window.title('my 8-bit avatar')

        self.current_avatar = {'skin_colors': 2,
                               'eye_color': 1,
                               'hair_color': 2,
                               'hair_style': 0,
                               'top_style': 0,
                               'beard': False,
                               'glasses': False
                               }

        self.canvas_left = Canvas(window, width=CANVAS_WIDTH, height=CANVAS_HEIGHT)
        self.canvas_left.pack(side=LEFT)

        self.frame_right = Frame(window,
                                 relief=RAISED,
                                 borderwidth=1,
                                 bg=FRAME_BACKGROUND_COLOR,
                                 width=BUTTON_FRAME_WIDTH,
                                 height=BUTTON_FRAME_HEIGHT
                                 )
        self.frame_right.pack(side=RIGHT, fill=BOTH, expand=True)

        self.buttons_label = Label(self.frame_right,
                                   bg=LABEL_COLOR,
                                   text='Click to Change:'
                                   )
        self.buttons_label.pack(side=TOP, fill=BOTH, expand=True, pady=10)

        self.skin_color_button = Button(self.frame_right,
                                        bg=BUTTON_COLOR,
                                        text='Skin Color',
                                        command=lambda:
                                        self.switch_to_next_feature_option('skin_colors', SKIN_COLORS)
                                        )
        self.skin_color_button.pack(side=TOP, fill=BOTH, expand=True)

        self.eye_color_button = Button(self.frame_right,
                                       bg=BUTTON_COLOR,
                                       text='Eye Color',
                                       command=lambda:
                                       self.switch_to_next_feature_option('eye_color', EYE_COLORS)
                                       )
        self.eye_color_button.pack(side=TOP, fill=BOTH, expand=True, pady=5)

        self.hair_color_button = Button(self.frame_right,
                                        bg=BUTTON_COLOR,
                                        text='Hair Color',
                                        command=lambda:
                                        self.switch_to_next_feature_option('hair_color', HAIR_COLORS)
                                        )
        self.hair_color_button.pack(side=TOP, fill=BOTH, expand=True)

        self.hair_style_button = Button(self.frame_right,
                                        bg=BUTTON_COLOR,
                                        text='Hair Style',
                                        command=lambda:
                                        self.switch_to_next_feature_option('hair_style', HAIR_STYLES)
                                        )
        self.hair_style_button.pack(side=TOP, fill=BOTH, expand=True, pady=5)

        self.top_style_button = Button(self.frame_right,
                                       bg=BUTTON_COLOR,
                                       text='Top Style',
                                       command=lambda:
                                       self.switch_to_next_feature_option('top_style', TOP_STYLES)
                                       )
        self.top_style_button.pack(side=TOP, fill=BOTH, expand=True)

        self.check_buttons_label = Label(self.frame_right,
                                         bg=LABEL_COLOR,
                                         text='Click to Add or Remove:'
                                         )
        self.check_buttons_label.pack(side=TOP, fill=BOTH, expand=True, pady=10)

        self.beard_button = Checkbutton(self.frame_right,
                                        bg=BUTTON_COLOR,
                                        text='Beard',
                                        command=lambda: self.change_checkbutton_state('beard')
                                        )
        self.beard_button.pack(side=TOP, fill=BOTH, expand=True)

        self.glasses_button = Checkbutton(self.frame_right,
                                          bg=BUTTON_COLOR,
                                          text='Glasses',
                                          command=lambda: self.change_checkbutton_state('glasses')
                                          )
        self.glasses_button.pack(side=TOP, fill=BOTH, expand=True, pady=5)

        self.close_button = Button(self.frame_right, bg=BUTTON_COLOR, text='Close', command=window.quit)
        self.close_button.pack(side=BOTTOM, fill=BOTH, expand=True)

        self.save_button = Button(self.frame_right, bg=BUTTON_COLOR, text='Save', command=self.save)
        self.save_button.pack(side=BOTTOM, fill=BOTH, expand=True, pady=5, anchor=CENTER)

        self.separator_label = Label(self.frame_right, bg=FRAME_BACKGROUND_COLOR)
        self.separator_label.pack(side=BOTTOM, fill=X, expand=True, pady=100)

        self.draw_avatar()

    # def clicked(self, button_feature, feature_list):
    #     self.switch_to_next_feature_option(button_feature, feature_list)
    #     self.draw_avatar()
    #
    # def checked(self, button_feature):
    #     self.change_checkbutton_state(button_feature)

    def switch_to_next_feature_option(self, button_feature, feature_list):
        last_index = len(feature_list) - 1
        current_index = self.current_avatar[button_feature]
        if last_index == current_index:
            self.current_avatar[button_feature] = 0
        else:
            self.current_avatar[button_feature] += 1
        self.draw_avatar()

    def change_checkbutton_state(self, button_feature):
        if self.current_avatar[button_feature]:
            self.current_avatar[button_feature] = False
        else:
            self.current_avatar[button_feature] = True
        self.draw_avatar()

    def draw_avatar(self):
        self.reset_canvas()
        draw_body(self.canvas_left,
                  SKIN_COLORS[self.current_avatar['skin_colors']],
                  EYE_COLORS[self.current_avatar['eye_color']])
        draw_hair(self.canvas_left,
                  HAIR_STYLES[self.current_avatar['hair_style']],
                  HAIR_COLORS[self.current_avatar['hair_color']])
        draw_top(self.canvas_left, TOP_STYLES[self.current_avatar['top_style']])
        if self.current_avatar['beard']:
            draw_beard(self.canvas_left, HAIR_COLORS[self.current_avatar['hair_color']])
        if self.current_avatar['glasses']:
            draw_glasses(self.canvas_left, HAIR_COLORS[self.current_avatar['hair_color']])

    def reset_canvas(self):
        self.canvas_left.delete('all')
        self.canvas_left.create_rectangle(0, 0, 805, 805, fill='#03fcd3', outline='#03fcd3')

    # the following function was borrowed from here: https://stackoverflow.com/a/34778636
    def save(self):
        ps = self.canvas_left.postscript(colormode='color')
        img = Image.open(io.BytesIO(ps.encode('utf-8')))
        img.save('./my_avatar.jpg')


def main():
    root = Tk()
    AvatarGui(root)
    root.mainloop()


if __name__ == '__main__':
    main()
