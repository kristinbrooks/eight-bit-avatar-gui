"""
avatar_draw_functions.py
-------------------------------------------
Author: Kristin Brooks
-------------------------------------------
This module contains the functions that draw the different parts of the avatars in eight_bit_avatar_gui_version.py
"""

# The following constants allow me to draw the avatar by using the number of pixels (squares) that make up the avatar
# starting from the base avatar's upper left corner position on the canvas
PIXEL_WIDTH = 50
PIXEL_HEIGHT = 50
STARTING_X = 200
STARTING_Y = 150


def draw_rows(canvas, x_range_start, x_range_end, pix_y, pixel_color):
    for pix_x in range(x_range_start, x_range_end):
        upper_left_x = STARTING_X + (pix_x * PIXEL_WIDTH)
        upper_left_y = STARTING_Y + (pix_y * PIXEL_HEIGHT)
        lower_right_x = STARTING_X + ((pix_x + 1) * PIXEL_WIDTH)
        lower_right_y = STARTING_Y + ((pix_y + 1) * PIXEL_HEIGHT)
        canvas.create_rectangle(upper_left_x, upper_left_y, lower_right_x, lower_right_y,
                                fill=pixel_color, outline=pixel_color)


def draw_columns(canvas, x_range_start, y_range_start, x_range_end, y_range_end, pixel_color):
    for pix_y in range(y_range_start, y_range_end):
        draw_rows(canvas, x_range_start, x_range_end, pix_y, pixel_color)


def draw_body(canvas, skin_colors_pair, eye_color):
    draw_head(canvas, skin_colors_pair)
    draw_columns(canvas, 0, 10, 1, 13, skin_colors_pair[0])  # left arm
    draw_columns(canvas, 7, 10, 8, 13, skin_colors_pair[0])  # right arm
    draw_eyes(canvas, eye_color)


def draw_head(canvas, skin_colors_pair):
    draw_columns(canvas, 2, 1, 6, 2, skin_colors_pair[0])  # forehead
    draw_columns(canvas, 0, 4, 1, 6, skin_colors_pair[0])  # left ear
    draw_columns(canvas, 1, 2, 7, 7, skin_colors_pair[0])  # central face
    draw_columns(canvas, 7, 4, 8, 6, skin_colors_pair[0])  # right ear
    draw_columns(canvas, 2, 7, 6, 8, skin_colors_pair[0])  # chin
    draw_columns(canvas, 3, 8, 5, 10, skin_colors_pair[0])  # neck
    draw_columns(canvas, 3, 6, 5, 7, skin_colors_pair[1])  # mouth


def draw_eyes(canvas, eye_color):
    draw_columns(canvas, 2, 4, 3, 5, eye_color)  # left eye
    draw_columns(canvas, 5, 4, 6, 5, eye_color)  # right eye


def draw_short_hair(canvas, hair_color):
    draw_columns(canvas, 0, 1, 1, 6, hair_color)  # far left side
    draw_columns(canvas, 1, 0, 2, 4, hair_color)
    draw_columns(canvas, 2, 0, 6, 3, hair_color)  # center
    draw_columns(canvas, 6, 0, 7, 4, hair_color)
    draw_columns(canvas, 7, 1, 8, 6, hair_color)  # far right side


def draw_medium_hair(canvas, hair_color):
    draw_columns(canvas, -2, 4, -1, 7, hair_color)  # far left side
    draw_columns(canvas, -1, 3, 0, 8, hair_color)
    draw_columns(canvas, 0, 1, 1, 9, hair_color)
    draw_columns(canvas, 1, 1, 2, 4, hair_color)
    draw_columns(canvas, 1, 7, 2, 8, hair_color)
    draw_columns(canvas, 2, 0, 6, 3, hair_color)  # center
    draw_columns(canvas, 6, 1, 7, 4, hair_color)
    draw_columns(canvas, 6, 7, 7, 8, hair_color)
    draw_columns(canvas, 7, 1, 8, 9, hair_color)
    draw_columns(canvas, 8, 3, 9, 8, hair_color)
    draw_columns(canvas, 9, 4, 10, 7, hair_color)  # far right side


def draw_long_hair(canvas, hair_color):
    draw_columns(canvas, 0, 2, 1, 10, hair_color)  # far left side
    draw_columns(canvas, 1, 1, 2, 4, hair_color)
    draw_columns(canvas, 1, 7, 2, 10, hair_color)
    draw_columns(canvas, 2, 8, 3, 9, hair_color)
    draw_columns(canvas, 2, 0, 6, 3, hair_color)  # center
    draw_columns(canvas, 5, 8, 6, 9, hair_color)
    draw_columns(canvas, 6, 7, 7, 10, hair_color)
    draw_columns(canvas, 6, 1, 7, 4, hair_color)
    draw_columns(canvas, 7, 2, 8, 10, hair_color)  # far right side


def draw_ponytail(canvas, hair_color):
    draw_columns(canvas, 0, 1, 1, 6, hair_color)  # far left side
    draw_columns(canvas, 1, 0, 2, 4, hair_color)
    draw_columns(canvas, 2, 0, 6, 3, hair_color)  # center
    draw_columns(canvas, 6, 0, 7, 4, hair_color)
    draw_columns(canvas, 7, 1, 8, 6, hair_color)
    draw_columns(canvas, 8, 2, 9, 4, hair_color)  # far right side
    draw_columns(canvas, 8, 4, 9, 6, '#FFFFFF')  # hair tie
    draw_columns(canvas, 9, 4, 10, 5, '#FFFFFF')  # hair tie
    draw_columns(canvas, 9, 5, 11, 9, hair_color)
    draw_columns(canvas, 8, 8, 10, 11, hair_color)  # ponytail
    draw_columns(canvas, 9, 11, 10, 12, hair_color)


def draw_pigtails(canvas, hair_color):
    draw_columns(canvas, -3, 1, -2, 4, hair_color)  # left pigtail
    draw_columns(canvas, -2, 1, -1, 5, hair_color)
    draw_columns(canvas, -1, 2, 1, 3, '#FFFFFF')  # hair tie
    draw_columns(canvas, 0, 1, 1, 5, hair_color)
    draw_columns(canvas, 1, 0, 2, 4, hair_color)
    draw_columns(canvas, 2, 0, 6, 3, hair_color)  # center
    draw_columns(canvas, 6, 0, 7, 4, hair_color)
    draw_columns(canvas, 7, 1, 8, 5, hair_color)
    draw_columns(canvas, 8, 2, 9, 3, '#FFFFFF')  # hair tie
    draw_columns(canvas, 9, 1, 10, 5, hair_color)
    draw_columns(canvas, 10, 1, 11, 4, hair_color)  # right pigtail


def draw_bald(canvas, hair_color):
    draw_columns(canvas, -1, 1, 0, 4, hair_color)  # far left side
    draw_columns(canvas, 0, 0, 1, 6, hair_color)
    draw_columns(canvas, 1, 0, 2, 3, hair_color)
    draw_columns(canvas, 6, 0, 7, 3, hair_color)
    draw_columns(canvas, 7, 0, 8, 6, hair_color)
    draw_columns(canvas, 8, 1, 9, 4, hair_color)  # far right side


def draw_mohawk(canvas, hair_color):
    draw_columns(canvas, 3, -2, 5, 3, hair_color)


def draw_beard(canvas, hair_color):
    canvas.create_rectangle(325, 425, 475, 475, fill=hair_color, outline=hair_color)  # over lips
    canvas.create_rectangle(350, 500, 450, 525, fill=hair_color, outline=hair_color)  # under lips
    canvas.create_rectangle(225, 475, 350, 525, fill=hair_color, outline=hair_color)  # left of mouth
    canvas.create_rectangle(450, 475, 575, 525, fill=hair_color, outline=hair_color)  # right of mouth
    canvas.create_rectangle(275, 525, 525, 575, fill=hair_color, outline=hair_color)  # chin cover


def draw_glasses(canvas, hair_color):
    frame_color = '#960101' if hair_color == '#000000' else '#000000'
    canvas.create_rectangle(275, 300, 360, 325, fill=frame_color, outline=frame_color)  # left: top
    canvas.create_rectangle(250, 325, 275, 425, fill=frame_color, outline=frame_color)  # left: left
    canvas.create_rectangle(275, 425, 360, 450, fill=frame_color, outline=frame_color)  # left: bottom
    canvas.create_rectangle(360, 325, 385, 425, fill=frame_color, outline=frame_color)  # left: right
    canvas.create_rectangle(385, 375, 415, 400, fill=frame_color, outline=frame_color)  # bridge
    canvas.create_rectangle(440, 300, 525, 325, fill=frame_color, outline=frame_color)  # right: top
    canvas.create_rectangle(415, 325, 440, 425, fill=frame_color, outline=frame_color)  # right: left
    canvas.create_rectangle(440, 425, 525, 450, fill=frame_color, outline=frame_color)  # right: bottom
    canvas.create_rectangle(525, 325, 550, 425, fill=frame_color, outline=frame_color)  # right: right


def draw_plain_shirt(canvas):
    draw_columns(canvas, 2, 9, 6, 10, '#6C6C6C')  # shirt collar
    draw_columns(canvas, 1, 10, 7, 13, '#6C6C6C')  # shirt body
    draw_columns(canvas, 0, 10, 1, 12, '#7C7C7C')  # left sleeve
    draw_columns(canvas, 7, 10, 8, 12, '#7C7C7C')  # right sleeve


def draw_striped_shirt(canvas):
    draw_columns(canvas, 2, 9, 6, 10, '#FFFFFF')  # shirt collar
    draw_columns(canvas, 1, 10, 7, 11, '#000000')  # shirt body
    draw_columns(canvas, 1, 11, 7, 12, '#FFFFFF')  # shirt body
    draw_columns(canvas, 1, 12, 7, 13, '#000000')  # shirt body


def draw_suspenders(canvas):
    draw_columns(canvas, 0, 10, 1, 12, '#8A8A8A')  # left sleeve
    draw_columns(canvas, 1, 10, 2, 13, '#C10000')  # left suspender
    draw_columns(canvas, 2, 9, 3, 10, '#A0A0A0')  # left collar
    draw_columns(canvas, 2, 10, 6, 13, '#A0A0A0')  # shirt center
    draw_columns(canvas, 5, 9, 6, 10, '#A0A0A0')  # right collar
    draw_columns(canvas, 6, 10, 7, 13, '#C10000')  # right suspender
    draw_columns(canvas, 7, 10, 8, 12, '#8A8A8A')  # right sleeve


def draw_jacket(canvas):
    draw_columns(canvas, 2, 9, 3, 10, '#3A3A3A')  # collar left
    draw_columns(canvas, 5, 9, 6, 10, '#3A3A3A')  # collar right
    draw_columns(canvas, 1, 10, 3, 13, '#666665')  # left side jacket
    draw_columns(canvas, 5, 10, 7, 13, '#666665')  # right side jacket
    draw_columns(canvas, 3, 10, 5, 13, '#FFFFFF')  # undershirt
    draw_columns(canvas, 0, 10, 1, 13, '#3A3A3A')  # left sleeve
    draw_columns(canvas, 7, 10, 8, 13, '#3A3A3A')  # right sleeve


def draw_top(canvas, top_style):
    switcher = {
        'plain': draw_plain_shirt,
        'striped': draw_striped_shirt,
        'suspenders': draw_suspenders,
        'jacket': draw_jacket
    }
    return switcher.get(top_style, lambda: 'Invalid top')(canvas)


def draw_hair(canvas, hair_style, hair_color):
    switcher = {
        'short': draw_short_hair,
        'medium': draw_medium_hair,
        'long': draw_long_hair,
        'ponytail': draw_ponytail,
        'pigtails': draw_pigtails,
        'bald': draw_bald,
        'mohawk': draw_mohawk
    }
    return switcher.get(hair_style, lambda: 'Invalid hair')(canvas, hair_color)
