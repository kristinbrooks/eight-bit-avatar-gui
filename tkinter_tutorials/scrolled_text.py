"""
Scrolled Text Widget

from: https://likegeeks.com/python-gui-examples-tkinter-tutorial/
"""
from tkinter import *
from tkinter import scrolledtext


window = Tk()
window.title('Welcome to LikeGeeks app')
window.geometry('350x200')

# must specify the width and height of the scrolled text widget or it will fill the whole window
txt = scrolledtext.ScrolledText(window, width=40, height=10)

# set the scrolledtext content
txt.insert(INSERT, 'This is a scrolledtext Widget.')

txt.grid(column=0, row=0)

# clear the contents of a scrolledtext widget
txt.delete(index1=0, index2=2)                  # NOT WORKING

window.mainloop()
