"""
Checkbutton Widget

from: https://likegeeks.com/python-gui-examples-tkinter-tutorial/
"""
from tkinter import *


window = Tk()
window.title('Welcome to LikeGeeks app')
window.geometry('350x200')

# BooleanVar() not a standard Python variable type, it's a Tkinter variable
# pass it to Checkbutton class to set the check state
chk_state = BooleanVar()

# set check state
# setting it to false will make it unchecked
chk_state.set(True)

# can also use IntVar() instead, it will give the same result as BooleanVar()
# chk_state = IntVar()

# chk_state.set(0)      # unchecked
# chk_state.set(1)      # checked


# create checkbutton widget
chk = Checkbutton(window, text='Choose', var=chk_state)

chk.grid(column=0, row=0)

window.mainloop()
