"""
Combobox class makes dropdown list widgets

from: https://likegeeks.com/python-gui-examples-tkinter-tutorial/
"""
from tkinter import *
from tkinter.ttk import Combobox


window = Tk()
window.title('Welcome to LikeGeeks app')
window.geometry('350x200')

# great the combobox
combo = Combobox(window)

# make the tuple of options that will show in teh dropdown
combo['values'] = ('Choose one', 1, 2, 3, 4, 5, 'Text')

# give it the index of the item that you want to show in the box when it renders
combo.current(0)

# set its location
combo.grid(column=0, row=0)

# to get the selected item use:
# combo.get()

window.mainloop()
