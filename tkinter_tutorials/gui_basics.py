"""
Shows How To:
- create windows
- make buttons
- add textboxes for user input

from: https://likegeeks.com/python-gui-examples-tkinter-tutorial/
"""
from tkinter import *
from tkmacosx import Button

# create a window
window = Tk()

# sets title on the top of the window
window.title('Welcome to LikeGeeks app')

# set window size with geometry function
window.geometry('350x200')

# create a label using the Label class
# set label font size/style by passing font parameters
lbl = Label(window, text='Hello')
# lbl = Label(window, text='Hello', font=('Arial Bold', 50))

# set the label's position on the form using the grid function
# the label won't show up without the grid function
lbl.grid(column=0, row=0)

#  the tkinter Entry class is a textbox so we can get user input
# add param `state='disabled'` to stop from being able to enter text
txt = Entry(window, width=10)
# it is placed in column 1 rather than 0, so it doesn't cover up the label
txt.grid(column=1, row=0)
# automatically gives focus to the entry widget so we don't have to click there to enter text
txt.focus()


# handling the click event for the button about to be created
def clicked():
    res = 'Welcome to ' + txt.get()
    lbl.configure(text=res)


# create a button in the window and give it a location
# 'fg' == foreground, 'bg' == background (these can be used with any widget)
# 'bg' on buttons specifically doesn't work on mac...this is why there is an extra import for tkmacosx
btn = Button(window, text='Click Me', command=clicked)
# btn = Button(window, text='Click Me', bg='orange', fg='red')
btn.grid(column=2, row=0)

# endless loop that keeps the window open waiting for user interaction until we close it
window.mainloop()
