"""
Radio Button widgets

from: https://likegeeks.com/python-gui-examples-tkinter-tutorial/
"""
from tkinter import *
from tkinter.ttk import *


window = Tk()
window.title('Welcome to LikeGeeks app')
window.geometry('350x200')

# create radio buttons, they need different values or they won't work right...they ones with the same value will both
# get marked when you click one of them
selected = IntVar()     # variable param to give to the radio buttons, so can get which one is selected later

rad1 = Radiobutton(window, text='First', value=1, variable=selected)

rad2 = Radiobutton(window, text='Second', value=2, variable=selected)

rad3 = Radiobutton(window, text='Third', value=3, variable=selected)


# can set the command to a specific function, so if the user clicks it the function code runs
def clicked():
    lbl = Label(window, text='You clicked the Fourth radio button.')
    lbl.grid(column=0, row=2)


rad4 = Radiobutton(window, text='Fourth', value=4, command=clicked)


# get the currently selected radio button or the radio button value using the variable param passed earlier
# every time you select a radio button
def clicked2():
    print(selected.get())


btn = Button(window, text='Click Me', command=clicked2())       # NOT WORKING

rad1.grid(column=0, row=0)

rad2.grid(column=1, row=0)

rad3.grid(column=3, row=0)

rad4.grid(column=4, row=0)

btn.grid(column=0, row=4)

window.mainloop()
